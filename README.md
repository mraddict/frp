# 个人FRP服务器和客户端配置文件

## 1. 启动服务端

进入服务配置文件夹：

```bash
cd frps
```

启动服务器：

```bash
docker-compose up -d
```

## 2. 启动客户端

进入客户端文件夹：

```bash
cd frpc
```

启动客户端：

```bash
docker-compose up -d
```
